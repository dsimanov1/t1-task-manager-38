package ru.t1.simanov.tm.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class ApplicationVersionRequest extends AbstractRequest {
}
