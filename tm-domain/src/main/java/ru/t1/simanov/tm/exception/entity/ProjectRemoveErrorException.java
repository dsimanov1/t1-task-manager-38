package ru.t1.simanov.tm.exception.entity;

public final class ProjectRemoveErrorException extends AbstractEntityNotFoundException {

    public ProjectRemoveErrorException() {
        super("Error! Failed to delete project by id...");
    }

}
