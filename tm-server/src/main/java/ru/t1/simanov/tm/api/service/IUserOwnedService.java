package ru.t1.simanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.api.repository.IUserOwnedRepository;
import ru.t1.simanov.tm.enumerated.Sort;
import ru.t1.simanov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M> {

    @NotNull
    List<M> findAll(
            @Nullable String userId,
            @Nullable Sort sort
    ) throws Exception;

    @Nullable
    M removeById(
            @Nullable String userId,
            @Nullable String id
    ) throws Exception;

    @Nullable
    M removeByIndex(
            @Nullable String userId,
            @Nullable Integer index
    ) throws Exception;

}
