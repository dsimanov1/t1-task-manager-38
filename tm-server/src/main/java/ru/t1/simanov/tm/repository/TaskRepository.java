package ru.t1.simanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.api.DBConstants;
import ru.t1.simanov.tm.api.repository.ITaskRepository;
import ru.t1.simanov.tm.enumerated.Status;
import ru.t1.simanov.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    protected String getTableName() {
        return DBConstants.TABLE_TASK;
    }

    @NotNull
    @Override
    public Task fetch(@NotNull final ResultSet row) throws Exception {
        @NotNull final Task task = new Task();
        task.setId(row.getString(DBConstants.COLUMN_ID));
        task.setName(row.getString(DBConstants.COLUMN_NAME));
        task.setCreated(row.getTimestamp(DBConstants.COLUMN_CREATED));
        task.setDescription(row.getString(DBConstants.COLUMN_DESCRIPTION));
        task.setUserId(row.getString(DBConstants.COLUMN_USER_ID));
        task.setStatus(Status.toStatus(row.getString(DBConstants.COLUMN_STATUS)));
        task.setProjectId(row.getString(DBConstants.COLUMN_PROJECT_ID));
        return task;
    }

    @NotNull
    @Override
    public Task add(@NotNull final Task task) throws Exception {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?)",
                getTableName(),
                DBConstants.COLUMN_ID,
                DBConstants.COLUMN_NAME,
                DBConstants.COLUMN_CREATED,
                DBConstants.COLUMN_DESCRIPTION,
                DBConstants.COLUMN_USER_ID,
                DBConstants.COLUMN_STATUS,
                DBConstants.COLUMN_PROJECT_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getId());
            statement.setString(2, task.getName());
            statement.setTimestamp(3, new Timestamp(task.getCreated().getTime()));
            statement.setString(4, task.getDescription());
            statement.setString(5, task.getUserId());
            statement.setString(6, Status.NOT_STARTED.name());
            statement.setString(7, task.getProjectId());
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    public Task add(
            @NotNull final String userId,
            @NotNull final Task task
    ) throws Exception {
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public void update(@NotNull final Task task) throws Exception {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBConstants.COLUMN_NAME, DBConstants.COLUMN_DESCRIPTION,
                DBConstants.COLUMN_STATUS, DBConstants.COLUMN_PROJECT_ID, DBConstants.COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getName());
            statement.setString(2, task.getDescription());
            statement.setString(3, task.getStatus().toString());
            statement.setString(4, task.getProjectId());
            statement.setString(5, task.getId());
            statement.executeUpdate();
        }
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception {
        @NotNull final Task task = new Task(name, description);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name
    ) throws Exception {
        @NotNull final Task task = new Task(name);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public List<Task> findAllTasksByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) throws Exception {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull String sql = String.format("SELECT * FROM %s WHERE %s = ? AND %s = ?",
                getTableName(), DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_PROJECT_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

}
