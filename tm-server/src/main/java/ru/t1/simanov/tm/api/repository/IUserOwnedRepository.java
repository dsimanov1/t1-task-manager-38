package ru.t1.simanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @NotNull
    M add(
            @NotNull String userId,
            @NotNull M model
    ) throws Exception;

    @NotNull
    List<M> findAll(@NotNull String userId) throws Exception;

    @NotNull
    List<M> findAll(
            @NotNull String userId,
            @Nullable Comparator comparator
    ) throws Exception;

    boolean existsById(
            @NotNull String userId,
            @NotNull String id
    ) throws Exception;

    @Nullable
    M findOneById(
            @NotNull String userId,
            @NotNull String id
    ) throws Exception;

    @Nullable
    M findOneByIndex(
            @NotNull String userId,
            @NotNull Integer index
    ) throws Exception;

    void clear(@NotNull String userId) throws Exception;

    @Nullable
    M remove(
            @NotNull String userId,
            @NotNull M model
    ) throws Exception;

    long getCount(@NotNull String userId) throws Exception;

}
