package ru.t1.simanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.simanov.tm.api.repository.IProjectRepository;
import ru.t1.simanov.tm.api.repository.ITaskRepository;
import ru.t1.simanov.tm.api.repository.IUserRepository;
import ru.t1.simanov.tm.api.service.IConnectionService;
import ru.t1.simanov.tm.api.service.IPropertyService;
import ru.t1.simanov.tm.comparator.NameComparator;
import ru.t1.simanov.tm.marker.UnitCategory;
import ru.t1.simanov.tm.model.Task;
import ru.t1.simanov.tm.model.User;
import ru.t1.simanov.tm.service.ConnectionService;
import ru.t1.simanov.tm.service.PropertyService;

import java.sql.Connection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static ru.t1.simanov.tm.constant.ProjectTestData.USER_PROJECT1;
import static ru.t1.simanov.tm.constant.ProjectTestData.USER_PROJECT2;
import static ru.t1.simanov.tm.constant.TaskTestData.*;
import static ru.t1.simanov.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.t1.simanov.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private static final IUserRepository userRepository = new UserRepository(connection);

    @NotNull
    private static String userId = "";

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository(connection);

    @NotNull
    private final ITaskRepository repository = new TaskRepository(connection);

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final User user = userRepository.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        userId = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final User user = userRepository.findByLogin(USER_TEST_LOGIN);
        if (user != null) userRepository.remove(user);
    }

    @Before
    public void before() throws Exception {
        projectRepository.add(userId, USER_PROJECT1);
        projectRepository.add(userId, USER_PROJECT2);
        repository.add(userId, USER_TASK1);
        repository.add(userId, USER_TASK2);
    }

    @After
    public void after() throws Exception {
        repository.clear(userId);
        projectRepository.clear(userId);
    }

    @Test
    public void addByUserId() throws Exception {
        Assert.assertNotNull(repository.add(userId, USER_TASK3));
        @Nullable final Task task = repository.findOneById(userId, USER_TASK3.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK3.getId(), task.getId());
        Assert.assertEquals(userId, task.getUserId());
    }

    @Test
    public void createByUserId() throws Exception {
        @NotNull final Task task = repository.create(userId, USER_TASK3.getName());
        Assert.assertEquals(USER_TASK3.getName(), task.getName());
        Assert.assertEquals(userId, task.getUserId());
    }

    @Test
    public void createByUserIdWithDescription() throws Exception {
        @NotNull final Task task = repository.create(userId, USER_TASK3.getName(), USER_TASK3.getDescription());
        Assert.assertEquals(USER_TASK3.getName(), task.getName());
        Assert.assertEquals(USER_TASK3.getDescription(), task.getDescription());
        Assert.assertEquals(userId, task.getUserId());
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        final List<Task> tasks = repository.findAll(userId);
        Assert.assertEquals(2, tasks.size());
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
    }

    @Test
    public void findAllComparatorByUserId() throws Exception {
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        final List<Task> tasks = repository.findAll(userId, comparator);
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
    }

    @Test
    public void findAllByProjectId() throws Exception {
        final List<Task> tasks = repository.findAllTasksByProjectId(userId, USER_PROJECT1.getId());
        Assert.assertEquals(2, tasks.size());
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
        tasks.forEach(task -> Assert.assertEquals(USER_PROJECT1.getId(), task.getProjectId()));
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(repository.existsById(NON_EXISTING_TASK_ID));
        Assert.assertTrue(repository.existsById(USER_TASK1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertFalse(repository.existsById(userId, NON_EXISTING_TASK_ID));
        Assert.assertTrue(repository.existsById(userId, USER_TASK1.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertNull(repository.findOneById(NON_EXISTING_TASK_ID));
        @Nullable final Task task = repository.findOneById(USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertNull(repository.findOneById(userId, NON_EXISTING_TASK_ID));
        @Nullable final Task task = repository.findOneById(userId, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void clearByUserId() throws Exception {
        repository.clear(userId);
        Assert.assertEquals(0, repository.getCount(userId));
    }

    @Test
    public void remove() throws Exception {
        @Nullable final Task removedTask = repository.remove(USER_TASK2);
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(USER_TASK2.getId(), removedTask.getId());
        Assert.assertNull(repository.findOneById(removedTask.getId()));
    }

    @Test
    public void removeByUserId() throws Exception {
        @Nullable final Task removedTask = repository.remove(userId, USER_TASK2);
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(USER_TASK2.getId(), removedTask.getId());
        Assert.assertNull(repository.findOneById(removedTask.getId()));
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertEquals(2, repository.getCount(userId));
    }

    @Test
    public void removeAll() throws Exception {
        repository.removeAll(TASK_LIST);
        Assert.assertEquals(0, repository.getCount(userId));
    }

    @Test
    public void update() throws Exception {
        USER_TASK1.setName(USER_TASK3.getName());
        repository.update(USER_TASK1);
        Assert.assertEquals(USER_TASK3.getName(), repository.findOneById(userId, USER_TASK1.getId()).getName());
    }

}
