package ru.t1.simanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.simanov.tm.api.repository.IUserRepository;
import ru.t1.simanov.tm.api.service.IConnectionService;
import ru.t1.simanov.tm.api.service.IProjectService;
import ru.t1.simanov.tm.api.service.IPropertyService;
import ru.t1.simanov.tm.comparator.NameComparator;
import ru.t1.simanov.tm.enumerated.Sort;
import ru.t1.simanov.tm.enumerated.Status;
import ru.t1.simanov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.simanov.tm.exception.field.*;
import ru.t1.simanov.tm.marker.UnitCategory;
import ru.t1.simanov.tm.model.Project;
import ru.t1.simanov.tm.model.User;
import ru.t1.simanov.tm.repository.UserRepository;

import java.sql.Connection;
import java.util.Comparator;
import java.util.List;

import static ru.t1.simanov.tm.constant.ProjectTestData.*;
import static ru.t1.simanov.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.t1.simanov.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private static final IUserRepository userRepository = new UserRepository(connection);
    @NotNull
    private static String userId = "";
    @NotNull
    private final IProjectService service = new ProjectService(connectionService);

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final User user = userRepository.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        userId = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final User user = userRepository.findByLogin(USER_TEST_LOGIN);
        if (user != null) userRepository.remove(user);
    }

    @Before
    public void before() throws Exception {
        service.add(userId, USER_PROJECT1);
        service.add(userId, USER_PROJECT2);
    }

    @After
    public void after() throws Exception {
        service.clear(userId);
    }

    @Test
    public void addByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.add(null, USER_PROJECT3);
        });
        Assert.assertNotNull(service.add(userId, USER_PROJECT3));
        @Nullable final Project project = service.findOneById(userId, USER_PROJECT3.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT3.getId(), project.getId());
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAll("");
        });
        final List<Project> projects = service.findAll(userId);
        Assert.assertEquals(2, projects.size());
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
    }

    @Test
    public void findAllComparatorByUserId() throws Exception {
        @Nullable Comparator comparator = null;
        Assert.assertNotNull(service.findAll(userId, comparator));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Comparator comparatorInner = null;
            service.findAll("", comparatorInner);
        });
        comparator = NameComparator.INSTANCE;
        final List<Project> projects = service.findAll(userId, comparator);
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
    }

    @Test
    public void findAllSortByUserId() throws Exception {
        @Nullable Sort sort = null;
        Assert.assertNotNull(service.findAll(userId, sort));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Sort sortInner = null;
            service.findAll("", sortInner);
        });
        sort = Sort.BY_NAME;
        final List<Project> projects = service.findAll(userId, sort);
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(service.existsById(""));
        Assert.assertFalse(service.existsById(NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(service.existsById(USER_PROJECT1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, NON_EXISTING_PROJECT_ID);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", NON_EXISTING_PROJECT_ID);
        });
        Assert.assertFalse(service.existsById(userId, ""));
        Assert.assertFalse(service.existsById(userId, NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(service.existsById(userId, USER_PROJECT1.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById("");
        });
        Assert.assertNull(service.findOneById(NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = service.findOneById(USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(userId, "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", USER_PROJECT1.getId());
        });
        Assert.assertNull(service.findOneById(userId, NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = service.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void clearByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.clear("");
        });
        service.clear(userId);
        Assert.assertEquals(0, service.getCount(userId));
    }

    @Test
    public void remove() throws Exception {
        @Nullable final Project removedProject = service.remove(USER_PROJECT2);
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(USER_PROJECT2, removedProject);
        Assert.assertNull(service.findOneById(USER_PROJECT2.getId()));
    }

    @Test
    public void removeByUserId() throws Exception {
        @Nullable final Project removedProject = service.remove(userId, USER_PROJECT2);
        Assert.assertEquals(USER_PROJECT2.getId(), removedProject.getId());
        Assert.assertNull(service.findOneById(userId, USER_PROJECT2.getId()));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(userId, null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(userId, "");
        });
        Assert.assertNull(service.removeById(userId, NON_EXISTING_PROJECT_ID));
        @Nullable final Project removedProject = service.removeById(userId, USER_PROJECT2.getId());
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(USER_PROJECT2.getId(), removedProject.getId());
        Assert.assertNull(service.findOneById(userId, USER_PROJECT2.getId()));
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.getCount("");
        });
        Assert.assertEquals(2, service.getCount(userId));
    }

    @Test
    public void create() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, USER_PROJECT3.getName());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", USER_PROJECT3.getName());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(userId, null);
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(userId, "");
        });
        @NotNull final Project project = service.create(userId, USER_PROJECT3.getName());
        Assert.assertEquals(project.getId(), service.findOneById(userId, project.getId()).getId());
        Assert.assertEquals(USER_PROJECT3.getName(), project.getName());
        Assert.assertEquals(userId, project.getUserId());
    }

    @Test
    public void createWithDescription() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, USER_PROJECT3.getName(), USER_PROJECT3.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", USER_PROJECT3.getName(), USER_PROJECT3.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(userId, null, USER_PROJECT3.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(userId, "", USER_PROJECT3.getDescription());
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.create(userId, USER_PROJECT3.getName(), null);
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.create(userId, USER_PROJECT3.getName(), "");
        });
        @NotNull final Project project = service.create(userId, USER_PROJECT3.getName(), USER_PROJECT3.getDescription());
        Assert.assertEquals(project.getId(), service.findOneById(userId, project.getId()).getId());
        Assert.assertEquals(USER_PROJECT3.getName(), project.getName());
        Assert.assertEquals(USER_PROJECT3.getDescription(), project.getDescription());
        Assert.assertEquals(userId, project.getUserId());
    }

    @Test
    public void updateById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById(null, USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById("", USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(userId, null, USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(userId, "", USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(userId, USER_PROJECT1.getId(), null, USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(userId, USER_PROJECT1.getId(), "", USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.updateById(userId, USER_PROJECT1.getId(), USER_PROJECT1.getName(), null);
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.updateById(userId, USER_PROJECT1.getId(), USER_PROJECT1.getName(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.updateById(userId, NON_EXISTING_PROJECT_ID, USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        @NotNull final String name = USER_PROJECT1.getName() + NON_EXISTING_PROJECT_ID;
        @NotNull final String description = USER_PROJECT1.getDescription() + NON_EXISTING_PROJECT_ID;
        service.updateById(userId, USER_PROJECT1.getId(), name, description);
        @Nullable final Project project = service.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

    @Test
    public void changeProjectStatusById() throws Exception {
        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeProjectStatusById(null, USER_PROJECT1.getId(), status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeProjectStatusById("", USER_PROJECT1.getId(), status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeProjectStatusById(userId, null, status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeProjectStatusById(userId, "", status);
        });
        Assert.assertThrows(StatusEmptyException.class, () -> {
            service.changeProjectStatusById(userId, USER_PROJECT1.getId(), null);
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.changeProjectStatusById(userId, NON_EXISTING_PROJECT_ID, status);
        });
        service.changeProjectStatusById(userId, USER_PROJECT1.getId(), status);
        @Nullable final Project project = service.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(status, project.getStatus());
    }

}
